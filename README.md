## Installation

1. Install the plugin using: **phonegap plugin add https://bitbucket.org/quimbik/phonegap-plugins-orientation**

2. Add the code snippets below:

- - -

Add the code below to AppDelegate.h:

	@property BOOL landscape;
		
	- (void)setOrientationToLandscape;
	- (void)setOrientationToPortrait;	

- - -

Change the supportedInterfaceOrientationsForWindow method on AppDelegate.m to this:

	- (NSUInteger)application:(UIApplication*)application supportedInterfaceOrientationsForWindow:(UIWindow*)window
	{
	    NSUInteger supportedInterfaceOrientations = (1 << UIInterfaceOrientationPortrait) | (0 << UIInterfaceOrientationLandscapeLeft) | (0 << UIInterfaceOrientationLandscapeRight) | (1 << UIInterfaceOrientationPortraitUpsideDown);
	    
	    if(self.landscape){
	        supportedInterfaceOrientations = (0 << UIInterfaceOrientationPortrait) | (1 << UIInterfaceOrientationLandscapeLeft) | (1 << UIInterfaceOrientationLandscapeRight) | (0 << UIInterfaceOrientationPortraitUpsideDown);
	    }

	    return supportedInterfaceOrientations;
	}

- - -

Add the code below to AppDelegate.m:

	- (void)setOrientationToLandscape
	{
	    self.landscape = TRUE;
	    
	    CGRect frame = [[UIScreen mainScreen] bounds];
	    [self.window setFrame:frame];
	    
	    UIViewController *vc = [[UIViewController alloc]init];
	    [self.window.rootViewController presentViewController:vc animated:NO completion:nil];
	    [self.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
	}

	- (void)setOrientationToPortrait
	{
	    self.landscape = FALSE;

	    CGRect frame = [[UIScreen mainScreen] bounds];
	    [self.window setFrame:frame];
	    
	    UIViewController *vc = [[UIViewController alloc]init];
	    [self.window.rootViewController presentViewController:vc animated:NO completion:nil];
	    [self.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
	}