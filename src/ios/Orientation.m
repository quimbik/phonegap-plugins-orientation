//
//  Orientation.m
//  Artkive
//
//  Created by Alessandro Jatoba on 6/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Orientation.h"
#import "AppDelegate.h"

@implementation Orientation

-(void)setOrientation:(CDVInvokedUrlCommand*)command
{
	NSString* orientation = (NSString*)[command.arguments objectAtIndex:0];
    AppDelegate* appDelegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
    if ([orientation isEqualToString:@"landscape"]){
    	[appDelegate setOrientationToLandscape];
    } else {
    	if ([orientation isEqualToString:@"portrait"]){
    		[appDelegate setOrientationToPortrait];
    	} 
    }    
}

@end