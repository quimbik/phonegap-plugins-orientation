var Orientation = function() {
}

Orientation.prototype.setOrientation = function(orientation) {
	cordova.exec(null, null, "Orientation", "setOrientation", [orientation]);
};

//-------------------------------------------------------------------

if(!window.plugins) {
    window.plugins = {};
}

if (!window.plugins.orientation) {
    window.plugins.orientation = new Orientation();
}

if (module.exports) {
    module.exports = Orientation;
}